package info.hccis.camper.bo;

import info.hccis.camper.dao.CamperTypeDAO;
import info.hccis.camper.jpa.entity.CamperType;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * CamperType business object
 *
 * @author CIS2232
 * @since 20201016
 */
public class CamperTypeBO {

    private static HashMap<Integer, CamperType> camperTypesMap = new HashMap();

    public static HashMap<Integer, CamperType> getCamperTypesMap() {
        return camperTypesMap;
    }

    public static void setCamperTypesMap(HashMap<Integer, CamperType> camperTypesMap) {
        CamperTypeBO.camperTypesMap = camperTypesMap;
    }



    
    
    
    
    /**
     * Connect to the data access object to get the campers from the datasource.
     *
     * @since 20200923
     * @author BJM
     */
    public ArrayList<CamperType> load() {

        //Read from the database
        CamperTypeDAO camperTypeDAO = new CamperTypeDAO();
        ArrayList<CamperType> camperTypes = camperTypeDAO.select();

        
        
        return camperTypes;
    }
}
