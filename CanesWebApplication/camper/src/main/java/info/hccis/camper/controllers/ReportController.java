package info.hccis.camper.controllers;

import info.hccis.camper.bo.CamperTypeBO;
import info.hccis.camper.entity.CamperReport;
import info.hccis.camper.jpa.entity.Camper;
import info.hccis.camper.jpa.entity.CamperType;
import info.hccis.camper.repositories.CamperRepository;
import java.util.ArrayList;
import java.util.HashMap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for the report functionality of the site
 *
 * @since 20201021
 * @author CIS2232
 */
@Controller
@RequestMapping("/report")
public class ReportController {

    private final CamperRepository camperRepository;

    public ReportController(CamperRepository cr) {
        camperRepository = cr;
    }

//    /**
//     * Page to allow user to view campers
//     *
//     * @since 20200528
//     * @author BJM (modified from Fred/Amro's project
//     */
//    @RequestMapping("/list")
//    public String list(Model model) {
//
//        //Go get the campers from the database.
//        //Use the jpa repository to get the campers.
//
//        model.addAttribute("campers", loadCampers());
//        model.addAttribute("findNameMessage", "Campers loaded");
//
//        return "camper/list";
//    }
    /**
     * Page to allow user to specify birthday
     *
     * @since 20201021
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/birthday")
    public String getBirthday(Model model) {

        CamperReport camperReport = new CamperReport();
        model.addAttribute("camperReport", camperReport);

        return "report/birthday";
    }

    /**
     * Page to allow user to submit the birthday report
     *
     * @since 20201021
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/submitBirthday")
    public String addSubmit(Model model, @ModelAttribute("camperReport") CamperReport camperReport) {

        //In here you would have all the attributes from the CamperReport object.  This
        //could be a start and end date.  Would then be able to use them to get the 
        //appropriate rows from the database.
        model.addAttribute("campers", loadCampersByDob(camperReport.getDateOfBirth()));
        return "camper/list";

    }

    public ArrayList<Camper> loadCampersByDob(String dob) {

        ArrayList<Camper> campers = (ArrayList<Camper>) camperRepository.findAllByDob(dob);
        HashMap<Integer, CamperType> camperTypesMap = CamperTypeBO.getCamperTypesMap();
        for (Camper current : campers) {
            if (current.getCamperType() != null) {
                current.setCamperTypeDescription(camperTypesMap.get(current.getCamperType()).getDescription());
            }
        }
        return campers;

    }

}
