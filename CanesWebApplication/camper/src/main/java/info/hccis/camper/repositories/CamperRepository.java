package info.hccis.camper.repositories;

import info.hccis.camper.jpa.entity.Camper;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CamperRepository extends CrudRepository<Camper, Integer> {
    
    ArrayList<Camper> findAllByDob(String dob);
    ArrayList<Camper> findAllByLastName(String lastName); //created but not used in project yet.
    
}