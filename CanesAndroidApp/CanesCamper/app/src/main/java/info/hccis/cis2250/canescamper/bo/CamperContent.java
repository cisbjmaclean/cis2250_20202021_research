package info.hccis.cis2250.canescamper.bo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import info.hccis.cis2250.canescamper.MainActivity;
import info.hccis.cis2250.canescamper.api.JsonCamperApi;
import info.hccis.cis2250.canescamper.ui.campers.CampersFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class CamperContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Camper> CAMPERS = new ArrayList<Camper>();


    /**
     * Reload the Room db with the provided list.
     *
     * @param campers
     * @author BJM
     * <p>
     * todo:  This should be on a background thread.
     * @since 20200207
     */

    public static void reloadCampersInRoom(List<Camper> campers) {
        //first delete all
        MainActivity.myAppDatabase.camperDAO().deleteAll();
        for (Camper current : campers) {
            MainActivity.myAppDatabase.camperDAO().add(current);
        }

    }

    /**
     * Get the list of campers that are currently in the Room database.
     *
     * @return the campers
     * @author BJM
     * @since 20200207
     */

    public static List<Camper> getCampersFromRoom() {

        Log.d("bjm get from db", "Loading from room.");

//        Camper newCamper = new Camper();
//        newCamper.setFirstName("Charlie");
//        newCamper.setLastName(("Smith"));

//        //Test the room add functionality
//        try {
//            MainActivity.myAppDatabase.camperDAO().add(newCamper);
//        } catch (Exception e) {
//            MainActivity.myAppDatabase.camperDAO().update(newCamper);
//        }
//        Log.d("bjm", "Added camper");

        List<Camper> campersBack = MainActivity.myAppDatabase.camperDAO().get();
        Log.d("bjm", "loaded from room db, size=" + campersBack.size());
        Log.d("bjm", "Here they are");
        for (Camper current : campersBack) {
            Log.d("bjm", current.toString());
        }
        Log.d("bjm", "that's it");

        return campersBack;


    }


    /**
     * Load the campers.  This method will use the rest service to provide the data.  The reason it is
     * in this class is because it is changing the value to the CAMPERS list.  This is the list which
     * is used to back the RecyclerView.
     *
     * @author BJM taken from Alex/Thomas' presentation.
     * @since 20200116
     */

    public static void loadCampers(Activity context) {

//        CamperContentVolley ccv = new CamperContentVolley();
//        //ccv.makeRequest(context, Util.CAMPER_BASE_API+"campers");
//        ccv.makeRequestPost(context, Util.CAMPER_BASE_API+"campers");







        Log.d("BJM", "Accessing api at:" + Util.CAMPER_BASE_API);

        //Use Retrofit to connect to the service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Util.CAMPER_BASE_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonCamperApi jsonCamperApi = retrofit.create(JsonCamperApi.class);

        //Create a list of campers.
        Call<List<Camper>> call = jsonCamperApi.getCampers();

        //final reference to activity to get shared preferences
        final Activity contextIn = context;

        call.enqueue(new Callback<List<Camper>>() {

            @Override
            public void onResponse(Call<List<Camper>> call, Response<List<Camper>> response) {

                List<Camper> campers;

                if (!response.isSuccessful()) {
                    Log.d("bjm rest", "not successful response from rest for campers Code=" + response.code());
                    campers = getCampersFromRoom();

                } else {


                    campers = response.body();
                    reloadCampersInRoom(campers);

                }
                Log.d("bjm", "data back from service call #returned=" + campers.size());

                //**********************************************************************************
                // Now that we have the campers, will use them to assign values to the list which
                // is backing the recycler view.
                //**********************************************************************************

                CamperContent.CAMPERS.clear();
                CamperContent.CAMPERS.addAll(campers);

                //**********************************************************************************
                // The CamperListFragment has a recyclerview which is used to show the camper list.
                // This recyclerview is backed by the camper list in the CamperContent class.  After
                // this list is loaded, need to notify the adapter from the recyclerview that the
                // data is changed.
                //**********************************************************************************

                CampersFragment.getRecyclerView().getAdapter().notifyDataSetChanged();

                //Remove the progress bar from the view.
                CampersFragment.clearProgressBarVisitiblity();

            }

            @Override
            public void onFailure(Call<List<Camper>> call, Throwable t) {

                //**********************************************************************************
                // If the api call failed, give a notification to the user.
                //**********************************************************************************
                Log.d("bjm", "api call failed");
                Log.d("bjm", t.getMessage());

                CamperContent.CAMPERS.clear();
                CamperContent.CAMPERS.addAll(getCampersFromRoom());

                CampersFragment.getRecyclerView().getAdapter().notifyDataSetChanged();

                //Remove the progress bar from the view.
                CampersFragment.clearProgressBarVisitiblity();


            }
        });
    }
}