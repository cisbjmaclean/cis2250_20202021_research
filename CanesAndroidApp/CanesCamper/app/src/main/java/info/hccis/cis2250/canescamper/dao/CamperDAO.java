package info.hccis.cis2250.canescamper.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import info.hccis.cis2250.canescamper.bo.Camper;


@Dao
public interface CamperDAO {

    @Insert
    public long add(Camper camper);

    @Update
    public void update(Camper camper);
    @Delete
    public void delete(Camper camper);

    @Query("select * from camper")
    public List<Camper> get();

    @Query("select * from camper where id =:id")
    public Camper get(int id);

    @Query("delete from camper")
    public void deleteAll();

}
