package info.hccis.cis2250.canescamper.bo;

import android.app.Activity;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import info.hccis.cis2250.canescamper.api.JsonCamperApi;
import info.hccis.cis2250.canescamper.ui.campers.CampersFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ApiWatcher class will be used as a background thread which will monitor the api. It will notify
 * the ui activity if the number of rows changes.
 *
 * @author BJM
 * @since 20210329
 */

public class ApiWatcher extends Thread {

    private int lengthLastCall = -1;  //Number of rows returned

    //The activity is passed in to allow the runOnUIThread to be used.
    private Activity activity = null;

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void run() {
        super.run();
        try {

            do {

                Thread.sleep(10000); //Check api every 10 seconds

                //************************************************************************
                // A lot of this code is duplicated from the CamperContent class.  It will
                // access the api and if if notes that the number of campers has changed, will
                // notify the CampersFragment that the data is changed.
                //************************************************************************

                //Use Retrofit to connect to the service
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Util.CAMPER_BASE_API)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                JsonCamperApi jsonCamperApi = retrofit.create(JsonCamperApi.class);

                //Create a list of campers.
                Call<List<Camper>> call = jsonCamperApi.getCampers();

                call.enqueue(new Callback<List<Camper>>() {

                    @Override
                    public void onResponse(Call<List<Camper>> call, Response<List<Camper>> response) {

                        List<Camper> campers = new ArrayList();

                        if (!response.isSuccessful()) {
                            Log.d("bjm rest", "not successful response from rest for campers Code=" + response.code());

                        } else {

                            campers = response.body();
                            int lengthThisCall = campers.size();

                            if (lengthLastCall == -1) {
                                //first time - don't notify
                                lengthLastCall = lengthThisCall;
                            } else if (lengthThisCall != lengthLastCall) {
                                //data has changed
                                Log.d("BJM background", "Data has changed");
                                lengthLastCall = lengthThisCall;

                                //**********************************************************************
                                // This method will allow a call to the runOnUiThread which will be allowed
                                // to interact with the ui components of the app.
                                //**********************************************************************
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        CampersFragment.notifyDataChanged("Hey - this is the background thread - the data has changed");
                                    }

                                });

                            } else {
                                Log.d("BJM background", "Data has NOT changed");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Camper>> call, Throwable t) {

                        //**********************************************************************************
                        // If the api call failed, give a notification to the user.
                        //**********************************************************************************
                        Log.d("bjm", "api call failed");
                        Log.d("bjm", t.getMessage());
                    }
                });

            } while (true);
        } catch (InterruptedException e) {
            Log.d("BJM","Thread interupted.  Stopping in the thread.");
        }


    }
}
