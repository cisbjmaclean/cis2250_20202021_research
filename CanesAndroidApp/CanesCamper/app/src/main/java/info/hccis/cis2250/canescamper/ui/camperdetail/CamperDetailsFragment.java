package info.hccis.cis2250.canescamper.ui.camperdetail;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import info.hccis.cis2250.canescamper.R;
import info.hccis.cis2250.canescamper.bo.Camper;


public class CamperDetailsFragment extends Fragment {

    private Camper camper;
    private TextView tvFirstName;
    private TextView tvLastName;
    private TextView tvDOB;
    private ImageButton buttonAddContact;
    private ImageButton buttonSendEmail;


    public static CamperDetailsFragment newInstance() {
        return new CamperDetailsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
          BJM 20200202
          Note how the arguments are passed from the activity to this fragment.  The
          camper object that was chosen on the recyclerview was encoded as a json string and then
          decoded when this fragment is accessed.
         */

        if (getArguments() != null) {
            String camperJson = getArguments().getString("camper");
            Gson gson = new Gson();
            camper = gson.fromJson(camperJson, Camper.class);
        }
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.camper_details_fragment, container, false);

        tvFirstName = view.findViewById(R.id.textViewDetailFirstName);
        tvLastName = view.findViewById(R.id.textViewDetailLastName);
        tvDOB = view.findViewById(R.id.textViewDetailDOB);

        tvFirstName.setText(camper.getFirstName());
        tvLastName.setText(camper.getLastName());
        tvDOB.setText(camper.getDob());

        return view;
    }

}
