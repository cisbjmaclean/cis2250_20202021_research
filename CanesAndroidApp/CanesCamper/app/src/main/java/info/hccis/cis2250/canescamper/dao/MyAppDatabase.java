package info.hccis.cis2250.canescamper.dao;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import info.hccis.cis2250.canescamper.bo.Camper;


@Database(entities = {Camper.class},version = 1)
public abstract class MyAppDatabase extends RoomDatabase {

    public abstract info.hccis.cis2250.canescamper.dao.CamperDAO camperDAO();

}
