package info.hccis.cis2250.canescamper.bo;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class CamperContentVolley {


    /**
     * Make a basic Vollege network call
     *
     * @param context
     * @param url
     * @author BJM
     * <p>
     * https://developer.android.com/training/volley/simple
     * @since 20210315
     */

    public void makeRequest(Context context, String url) {

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("BJM Volley", "Volley responded..." + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BJM", "BJM volley error:" + error.getMessage());
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);


    }

    /**
     * Make a post request
     *
     * @param context
     * @param url     location of service
     *                <p>
     *                This method will call a service passing a JsonObject to it to allow a post to take place.
     * @author BJM
     * <p>
     * https://nabeelj.medium.com/making-a-simple-get-and-post-request-using-volley-beginners-guide-ee608f10c0a9
     * @since 20210315
     */

    public void makeRequestPost(Context context, String url) {

        Gson gson = new Gson();

        Camper camper = new Camper();
        camper.setId(0);
        camper.setFirstName("BJ745");
        camper.setLastName("MacLean");
        String camperJson = gson.toJson(camper);

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);

        JSONObject postData = null;
        try {
            postData = new JSONObject(camperJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//            try {
//                postData.put("camperJson", camperJson);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, postData, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("BJM back from post", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BJM back from post", "Error"+error.toString());
                error.printStackTrace();
            }
        });

        queue.add(jsonObjectRequest);

    }


}
