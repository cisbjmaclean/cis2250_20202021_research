package info.hccis.cis2250.canescamper.ui.feedback;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;

import info.hccis.cis2250.canescamper.R;

public class FeedbackFragment extends Fragment {

    private FeedbackViewModel feedbackViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        feedbackViewModel =
                new ViewModelProvider(this).get(FeedbackViewModel.class);
        View root = inflater.inflate(R.layout.fragment_feedback, container, false);
        //final TextView textView = root.findViewById(R.id.text_feedback);
        feedbackViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        /*
        * Set on click listener when click on submit in feedback fragment to show rating value
        * Date: 2021/01/23
        * Purpose: used for material design presentation - get value of rating bar and display an snackbar with an action button
        */
        RatingBar ratingBar = root.findViewById(R.id.rating_bar);
        Button btnRate = root.findViewById(R.id.btn_rate);

        btnRate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String ratingValue = String.valueOf(ratingBar.getRating());

                Snackbar snackbar = Snackbar.make(root, ratingValue + " Stars", Snackbar.LENGTH_LONG).setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar snackbar1 = Snackbar.make(view, "Rate is restored!", Snackbar.LENGTH_LONG);
                        snackbar1.show();
                    }
                });
                snackbar.setActionTextColor(Color.CYAN);
                snackbar.show();

            }
        });
        return root;
    }
}