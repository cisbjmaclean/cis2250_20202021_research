package info.hccis.cis2250.canescamper.ui.home;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import info.hccis.cis2250.canescamper.R;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        //final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }

        });

        /*
         * Set on click listener when click on submit in Home fragment to check for empty first and last name
         * Date: 2021/01/23
         * Purpose: used for material design presentation - display an alert dialog
         */
        Button button = root.findViewById(R.id.btnSubmit);
        EditText editTextViewFirstName = root.findViewById(R.id.editTextTextCamperFirstName);
        EditText editTextViewLastName = root.findViewById(R.id.editTextTextCamperLastName);

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(editTextViewFirstName.getText().toString().isEmpty() || editTextViewLastName.getText().toString().isEmpty()){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                    // Setting Dialog Title
                    alertDialog.setTitle("ERROR");
                    // Setting Dialog Message
                    alertDialog.setMessage("Missing information ...");
                    // Setting Icon to Dialog
                    alertDialog.setIcon(R.drawable.ic_error);
                    // Setting "Submit" Btn
                    alertDialog.setNeutralButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    alertDialog.show();
                } else {
                    Toast toast = Toast.makeText(getContext(),  editTextViewFirstName.getText().toString() +" "+ editTextViewLastName.getText().toString()+ " is added",Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP,0,250);
                    toast.show();
                }
            }
        });

        /*
         * Set on click listener when click on the rate button in the home fragment
         * Date: 2021/01/23
         * Purpose: used for material design presentation - navigate to Feedback fragment
         */
        Button btn = root.findViewById(R.id.btnRate);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.nav_feedback);
            }
        });

       return root;
    }
}